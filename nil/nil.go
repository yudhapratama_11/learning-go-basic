package main

import "fmt"

func NewMap(name string) map[string]string {
	if name == "" {
		return nil
	} else {
		return map[string]string{
			"name": name,
		}
	}
}

func main() {
	mapData := NewMap("Yudha")
	fmt.Println(mapData)

	mapData1 := NewMap("")
	fmt.Println(mapData1)

	var person map[string]string = nil
	person = NewMap("")
	if person == nil {
		fmt.Println("Data kosong")
	} else {
		fmt.Println(person)
	}
}
