package main

import "fmt"

func main() {
	var names [3]string
	names[0] = "Depan"
	names[1] = "Tengah"
	names[2] = "Belakang"

	fmt.Println(names[0] + " " + names[1] + " " + names[2])

	// Initiate value langsung
	var values = [3]int{
		1,
		2,
		5,
	}
	fmt.Println(values)
	fmt.Println(len(values)) // length of array
}
