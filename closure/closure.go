package main

import "fmt"

type Blacklist func(string) bool

func main() {
	counter := 0

	increment := func() { // Mirip Global Variabel
		fmt.Pri ntln("Increment")
		counter++
	}

	fmt.Println(counter)

	increment()
	increment()

	fmt.Println(counter)
}
