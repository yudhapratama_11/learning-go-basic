package database // kalau dalam 1 package gaboleh fungsi dengan nama yang sama dengan file lain

var connection string

func init() { // Dia menjalankan ini duluan ketika start
	connection = "MySQL"
}

func GetDatabase() string {
	return connection
}
