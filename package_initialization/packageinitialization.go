package main

import (
	"belajar-golang/Part_46/database"
	// _ "belajar-golang/Part_46/database" //ini kalau misalnya packagenya ga mau dipake, mirip command gitu
	"fmt"
)

func main() {
	db := database.GetDatabase()
	fmt.Println(db)
}
