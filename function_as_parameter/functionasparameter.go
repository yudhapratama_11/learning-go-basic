package main

import "fmt"

type Filter func(string) string

func main() {
	sayHelloWithFilter("Yudha", filterKata)

	filter := filterKata
	sayHelloWithFilter("Anjing", filter)

	sayHelloWithFilterUsingTypeDeclaration("Keren", filter)
}

func sayHelloWithFilter(name string, filter func(string) string) {
	nameFiltered := filter(name)
	fmt.Println("Hello", nameFiltered)
}

// Filternya pakai type Filter diline 5 pengganti tipe data dan parameter``
func sayHelloWithFilterUsingTypeDeclaration(name string, filter Filter) {
	nameFiltered := filter(name)
	fmt.Println("Hello", nameFiltered)
}

func filterKata(kata string) string {
	if kata == "Anjing" {
		return "..."
	}
	return kata
}
