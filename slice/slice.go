package main

import "fmt"

func main() {

	names := [...]string{"Yudha", "Pratama", "Joko", "Andi"}
	slice := names[2:4]

	fmt.Println(slice[0])
	fmt.Println(slice[1])

	months := [...]string{"Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"}
	fmt.Println(months)

	var slice1 = months[2:4]
	fmt.Println(slice1)
	fmt.Println(len(slice1)) // panjang slice
	fmt.Println(cap(slice1)) // kapasitas slice (2 sampe 12 = 10)

	months[5] = "Keubah" // Pas Array diubah, slice juga ikut diubah
	fmt.Println(slice1)

	/////APEND SLICE/////////
	days := [...]string{"Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"}
	daysSlice := days[5:]
	fmt.Println(cap(daysSlice))
	daysSlice[0] = "Saturday"
	daysSlice[1] = "Sunday"

	fmt.Println(days)      // [Senin, Selasa, Rabu, Kamis, Jumat, Saturday, Sunday]
	fmt.Println(daysSlice) // [Saturday, Sunday]

	daysSlice2 := append(daysSlice, "Libur")
	fmt.Println(daysSlice2) // [Saturday, Sunday, Libur]
	// fmt.Println(len(daysSlice2))
	// fmt.Println(cap(daysSlice2))
	// Alasan kenapa 4 karena dia menambahkan jumlah array yang days slice jadi length 2 + length 2, tapi yang array 4 harus di deklarasi dulu baru bisa dipake dengan cara append
	fmt.Println()

	newSlice := make([]string, 2, 5) //array(tipe), length, capacity(optional)
	newSlice[0] = "Yudha"
	newSlice[1] = "Pratama"
	fmt.Println(newSlice)
	fmt.Println(len(newSlice))
	fmt.Println(cap(newSlice))
	fmt.Println()

	//COPY SLICE//
	copySlice := make([]string, len(newSlice), cap(newSlice))
	copy(copySlice, newSlice)
	fmt.Println(copySlice)
	fmt.Println()

	//Perbedaan Slice dan Array
	iniArray := [...]int{1, 2, 3, 4, 5}
	iniSlice := []int{1, 2, 3, 4, 5}
	fmt.Println(iniArray)
	fmt.Println(iniSlice)
	iniSlice = append(iniSlice, 3) // Kalau slice bisa diappend karena lengthnya bersifat dinamis, sedangkan array tidak
}
