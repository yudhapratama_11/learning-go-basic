package main

import (
	"errors"
	"fmt"
)

func Pembagian(nilai1 int, nilai2 int) (int, error) {
	if nilai2 == 0 {
		return 0, errors.New("Pembagian dengan NOL")
	} else {
		return nilai1 / nilai2, nil
	}
}

func main() {
	var tesError error = errors.New("Ups, error") // tipe data error, memanggil fungsi errors yang merupakan fungsi dari golang
	fmt.Println(tesError)

	angka, error := Pembagian(1, 0)
	if error == nil {
		fmt.Println(angka)
	} else {
		fmt.Println(error)
		fmt.Println(error.Error()) // bisa panggil gini juga
	}

}
