package main

import "fmt"

func main() {
	person := map[string]interface{}{
		"name":     "Yudha",
		"address":  "Rahasia",
		"nomorktp": 123,
	}

	person["title"] = "Programmer" //bisa ditambah key baru, mirip seperti object

	fmt.Println(person)
	fmt.Println(person["name"])
	fmt.Println(person["address"])
	fmt.Println(len(person))
	fmt.Println()

	book := make(map[string]string) // Inisialisasi map kosong
	book["title"] = "Belajar Go-Lang"
	book["author"] = "Master"
	book["isi"] = "Yes"
	fmt.Println(book)
	fmt.Println()

	delete(book, "isi")
	fmt.Println(book)
}
