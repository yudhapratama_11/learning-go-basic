package main

import "fmt"

func main() {
	var nilai32 int32 = 32768
	var nilai64 int64 = int64(nilai32)

	// Kalau mencapai batas angka maka dia akan ke batas bawah, misal 128 maksimal terus ke -128 (kalau int 8 itu -128 s/d 128)
	var nilai16 int16 = int16(nilai32)
	fmt.Println(nilai32)
	fmt.Println(nilai64)
	fmt.Println(nilai16)

	var name = "Yudha"
	var nameByte byte = name[0] // Ambil huruf y dengan tipe data byte
	var eString string = string(nameByte)

	fmt.Println(eString)

}
