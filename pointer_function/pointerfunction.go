package main

import (
	"fmt"
)

type Address struct {
	City, Province, Country string
}

func main() {
	var address Address = Address{"Jakarta Utara", "DKI Jakarta", ""} // variabel biasa tanpa pointer
	fmt.Println("Awal", address)

	ChangeAddressToIndonesia(&address)
	fmt.Println("Akhir", address)

	// var vars = 789

	// // pointer for var
	// var ptr2 *int // store dengan address 4

	// // double pointer for ptr2
	// var ptr1 **int // store dengan address 8

	// // storing address of var in ptr2
	// ptr2 = &vars

	// // Storing address of ptr2 in ptr1
	// ptr1 = &ptr2

	// // Displaying value of var using
	// // both single and double pointers
	// fmt.Printf("Value of var = %d\n", vars)
	// fmt.Printf("Value of var using single pointer = %d\n", *ptr2)  // kalau ga pake bintang, ditampilkan addressnya
	// fmt.Printf("Value of var using double pointer = %d\n", **ptr1) // kalau bintang1, dia reference ke ptr2, kalau gada bintang reference ke variable vars

	// var angka int
	// angka = 1
	// test(&angka)
	// fmt.Println(angka, "test")

	// test := "hi"
	// fmt.Printf("\nAddress of variable: %v\n", &test)
	// fmt.Println("============================")

	// var actualVar string = "Australia" /* variable declaration */
	// var actualVar1 string = "Australia"
	// var pointerVar *string /* pointer variable declaration */
	// var pointerVar1 *string
	// /* store address of actual variable in pointer variable*/
	// pointerVar = &actualVar
	// pointerVar1 = &actualVar1

	// fmt.Printf("\nAddress of variable: %v", &actualVar)
	// fmt.Printf("\nAddress stored in pointer variable: %v", pointerVar)

	// fmt.Printf("\nValue of Actual Variable: %s", actualVar)
	// fmt.Printf("\nValue of Pointer variable: %s", *pointerVar)

	// fmt.Printf("\nAddress of variable: %v", &actualVar1)
	// fmt.Printf("\nAddress stored in pointer variable: %v", pointerVar1)

	// fmt.Printf("\nValue of Actual Variable: %s", actualVar1)
	// fmt.Printf("\nValue of Pointer variable: %s", *pointerVar1)
}

func ChangeAddressToIndonesia(address *Address) { // wajib pake bintang karena mengambil address dari variabel address diatas
	address.Country = "Indonesia"
	fmt.Println("Tengah", address) // Bakalan keubah karena kita set reference address ke pointer data1, jadinya address itu ikut perubahan dari data1

	// var data1 *Address = address // dia mengambil addressnya dari parameter address diatas
	// data1.Country = "Indonesia"
	// fmt.Println("Tengah", data1) // Bakalan keubah karena kita set reference address ke pointer data1, jadinya address itu ikut perubahan dari data1

	//=================== YANG TIDAK BERUBAH DIBAWAH =======================

	// // fmt.Printf("Value of var = %v\n", *address)
	// var data Address = *address // *address = dia mengambil value dari pointer itu, jadi hanya value saja yang didapatkan bukan address
	// data.Country = "Indonesia"
	// fmt.Println("Tengah", data) // Beda address karena data sudah dibuatkan address baru
}

// func test(angka *int) { // wajib pake bintang karena mengambil address dari variabel address diatas
// 	var data *int = angka
// 	var angkabaru int = *data + 1
// 	*data = angkabaru // diset nilai dari pointer angka baru referencenya ke address pointer data
// 	fmt.Println(angkabaru, "variabel lokal karena tidak diset")
// }
