package main

import "fmt"

func main() {
	runApplicationPanic(true)
}

func runApplicationPanic(error bool) {
	defer endApplicationPanic() // Ditaruh diatas, tapi dieksekusi setelah baris kode paling bawah selesai
	if error == true {
		panic("Aplikasi error")
	}
	fmt.Println("Proses aplikasi sedang dijalankan")
}

func endApplicationPanic() {
	fmt.Println("Selesai mengeksekusi aplikasi")
}
