package main

import "fmt"

func mains() {
	runApplicationRecover(true)
}

func runApplicationRecover(error bool) {
	defer endApplicationRecover() // Ditaruh diatas, tapi dieksekusi setelah baris kode paling bawah selesai
	if error == true {
		panic("Aplikasi error")
	}

}

func endApplicationRecover() {
	message := recover() // pengunaan recover seperti log try catch
	if message != nil {
		fmt.Println("Terdapat Error =", message)
	}

	fmt.Println("Selesai mengeksekusi aplikasi")
}
