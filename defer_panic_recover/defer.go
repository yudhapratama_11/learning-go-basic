package main

import "fmt"

func mainss() {
	runApplicationDefer(10)
}

func logging() {
	fmt.Println("Sukses memanggil function")
}

func runApplicationDefer(value int) {
	defer logging() // Ditaruh diatas, tapi dieksekusi setelah baris kode paling bawah selesai
	fmt.Println("Menjalankan Function")
	result := 10 / value
	fmt.Println("Result:", result)

}
