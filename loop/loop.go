package main

import "fmt"

func main() {
	counter := 1

	for counter <= 10 {
		fmt.Println(counter)
		counter++
	}

	// for counter1 := 1; counter1 <= 10; counter1++ {
	// 	fmt.Println(counter1)
	// }

	fmt.Println()

	slice := []string{"Yudha", "Pratama"}
	for i := 0; i < len(slice); i++ {
		fmt.Println(slice[i])
	}

	for _, name := range slice {
		fmt.Println("Nama = " + name)
		//fmt.Println("index", index, "name", name)
	}

	person := make(map[string]string)
	person["name"] = "Yudha"
	person["title"] = "Programmer"

	for key, value := range person {
		fmt.Println("Key = ", key, "Value = ", value)
	}
}
