package main

import "fmt"

type HasName interface {
	GetName() string // Fungsi GetName wajib diset
	GetId(angka int) int
}

func (animal Animal) GetName() string {
	return animal.Name
}

func (animal Animal) GetId(angka int) int {
	return animal.Id + angka
}

type Animal struct {
	Name string
	Id   int
}

func SayHello(name HasName, angka int) { // Seakan2 struct customer punya fungsi sayHello
	fmt.Println("Ini adalah", name.GetName())
	fmt.Println("Id", name.GetId(angka))
}

func main() {
	animal := Animal{
		Name: "Burung",
		Id:   1,
	}

	SayHello(animal, 2)

}
