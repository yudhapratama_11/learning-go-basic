package main

import "fmt"

type HasName interface {
	GetName() string // Fungsi GetName wajib diset dan harus mempunyai fungsi ini
	GetID() int      // Fungsi GetAnother wajib diset dan harus mempunyai fungsi ini
}

func (person Person) GetName() string {
	// Gender
	return person.Name
}

func (animal Animal) GetName() string {
	// Jenis
	return animal.Name
}

func (animal Animal) GetID() int {
	return animal.Id
}

func (person Person) GetID() int {
	return person.Id
}

type Person struct {
	Name string
	Id   int
}

type Animal struct {
	Name string
	Id   int
}

// Seakan2 struct customer punya fungsi sayHello
func SayHello(name HasName) {
	fmt.Println("Hello", name.GetName())
	//fmt.Println("Hello", name.GetAnother())
}

func main() {
	var personUser Person
	personUser.Name = "Yudha"
	personUser.Id = 1

	SayHello(personUser)

	fmt.Println("Hello", personUser.GetName())

	animal := Animal{
		Name: "Kucing",
	}

	SayHello(animal)
}
