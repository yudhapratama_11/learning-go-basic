package main

import "fmt"

func main() {
	name := "Yudha"

	switch name {
	case "Yudha":
		fmt.Println("Hello Yudha")
	case "Pratama":
		fmt.Println("Hello Pratama")
	default:
		fmt.Println("Name is different")
	}
}
