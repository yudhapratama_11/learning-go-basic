package main

import "fmt"

type Customer struct {
	Name, Address string
	Age           int
}

func main() {
	var dataCust Customer // Cara 1
	dataCust.Name = "Yudha Pratama"
	dataCust.Address = "Jakarta"
	dataCust.Age = 20

	dataCust1 := Customer{ // Cara 2
		Name:    "Joko",
		Address: "Jogja",
		Age:     25,
	}

	fmt.Println(dataCust)
	fmt.Println(dataCust.Name)

	fmt.Println(dataCust1.Age)

}
