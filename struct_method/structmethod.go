package main

import "fmt"

type Customer struct {
	Name, Address string
	Age           int
}

type Product struct {
	Name string
	Id   int
}

func sayHi(cust Customer) { // Seakan2 struct customer punya fungsi sayHello
	fmt.Println("Hi, My Name is", cust.Name)
}

func (cust Customer) sayHello() { // Seakan2 struct customer punya fungsi sayHello
	fmt.Println("Hello, My Name is", cust.Name)
}

func GetProduct(prod Product) { // Seakan2 struct customer punya fungsi sayHello
	fmt.Println("Hello, My Name is", prod.Name)
}

func (prod Product) GetProductWithStruct(desc string) { // Seakan2 struct customer punya fungsi sayHello
	fmt.Println("Product ", prod.Name+desc)
}

func main() {
	var dataCust Customer // Cara 1
	dataCust.Name = "Yudha Pratama"
	dataCust.Address = "Jakarta"
	dataCust.Age = 20

	prod := Product{
		Name: "Produk 1",
		Id:   1,
	}

	prod.GetProductWithStruct("Tambahan")

	prod = Product{
		Name: "Produk 2",
		Id:   2,
	}

	prod.GetProductWithStruct("Tambahan")

	GetProduct(prod)

	dataCust.sayHello()

	dataCust1 := Customer{ // Cara 2
		Name:    "Joko",
		Address: "Jogja",
		Age:     25,
	}

	sayHi(dataCust1) // Pengunaan sayhello diatas sama dengan ini

}
