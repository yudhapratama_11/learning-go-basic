package main

import (
	"flag"
	"fmt"
)

func main() {
	var host *string = flag.String("host", "localhost", "Put your database host") // key, value, description
	username := flag.String("username", "root", "Put your database host")         // key, value, description
	password := flag.String("password", "root", "Put your database host")         // key, value, description
	number := flag.Int("number", 1, "Put your number")                            // key, value, description

	flag.Parse()

	fmt.Println(*host, *username, *password, *number)

	//runnya go run packageflag.go -host=localhost -username=username -password=password (kalau gamau pake yang default, mau pake flag sendiri)
}
