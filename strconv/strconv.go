package main

import (
	"fmt"
	"strconv"
)

func main() {
	boolean, err := strconv.ParseBool("true") // Parse dari string ke boolean (harus true / false)

	if err == nil {
		fmt.Println(boolean)
	} else {
		fmt.Println(err)
	}

	number, err := strconv.ParseInt("19", 10, 32) // parse dari string to int (angka, tipedatanya apa (binary = 2, decimal = 10, hexadecimal = 16), tipe int(32/64))
	if err == nil {
		fmt.Println(number)
	} else {
		fmt.Println(err)
	}

	value := strconv.FormatInt(10000, 10) // dari int ke string (nilai, tipedatanya mau jadi apa (binary = 2, decimal = 10, hexa = 16))
	fmt.Println(value)

	value1, err1 := strconv.Atoi("10000") // Sama ky parseint
	if err1 == nil {
		fmt.Println(value1)
	} else {
		fmt.Println(err)
	}

	value2 := strconv.Itoa(100001) // int to string
	fmt.Println(value2)
}
