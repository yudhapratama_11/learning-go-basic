package main

import (
	"fmt"
	"time"
)

func main() {
	//fmt.Println(time.Date(2020, 12, 10, 25, 0, 0, 0, time.Now().Location()))

	now := time.Now()

	fmt.Println(now)
	fmt.Println(now.Local())
	fmt.Println(now.Local().Year())

	utc := time.Date(2020, time.November, 10, 25, 10, 30, 00, time.UTC)
	fmt.Println(utc)

	// time.RFC3339
	// layout := time.RFC3339 // Harus pake format RFC3339 (tidak bisa yyyy-mm-dd)
	layout := "2006-01-02 15:04"
	parse, _ := time.Parse(layout, "2020-10-01 10:32")
	fmt.Println(parse.AddDate(0, 0, 1))
}
