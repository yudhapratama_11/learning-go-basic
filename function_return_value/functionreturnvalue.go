package main

import "fmt"

func main() {
	result := getHello("Yudha", "Pratama")
	fmt.Println(result)
}

func getHello(firstName string, lastName string) string {
	return "Hello " + firstName + " " + lastName
}
