package main

import (
	"container/list"
	"fmt"
)

func main() {
	data := list.New() // Sama ky linked list
	data.PushBack("Yudha")
	data.PushBack("Pratama")
	data.PushFront("Awal")
	data.PushBack("Latihan")

	for e := data.Front(); e != nil; e = e.Next() {
		fmt.Println(e.Value)
	}

	fmt.Println(data.Front().Value)
	fmt.Println(data.Front().Next().Value)
	fmt.Println(data.Back().Value)
	fmt.Println(data.Back().Prev().Value)
}
