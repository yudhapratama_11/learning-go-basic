package main

import "fmt"

func main() {
	var name string
	name = "Yudha Pratama"
	fmt.Println(name)

	// kalo langsung dideklarasi valuenya tidak perlu inisialisasi tipe datanya
	var variabel = 10
	fmt.Println(variabel)

	variabel1 := true
	fmt.Println(variabel1)

	var variabel2 string = "test"
	fmt.Println(variabel2)

	// Declare Multiple Variable
	var (
		firstName = "Yudha"
		lastName  = "Pratama"
	)
	fmt.Println(firstName + " " + lastName)
}
