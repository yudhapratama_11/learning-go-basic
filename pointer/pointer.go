package main

import (
	"fmt"
)

type Address struct {
	City, Province, Country string
}

func main() {
	//KONSEP DETAIL PASS BY VALUE
	address1 := Address{"Subang", "Jawa Barat", "Indonesia"}
	address2 := address1 // copy value

	address2.City = "Bandung"

	fmt.Println(address1) //  address.city disini tidak berubah jadi bandung
	fmt.Println(address2)
	fmt.Println("====================================")

	//KONSEP DETAIL PASS BY REFERENCE
	var address3 Address = Address{"Subang", "Jawa Barat", "Indonesia"}
	var address4 *Address = &address3 // Kalau begini dia mereference ke address 3

	address4.City = "Bandung"

	fmt.Println(address3) //  address.city disini akan berubah jadi bandung address4 mempunyai address sama dengan address3
	fmt.Println(address4)
	fmt.Println("====================================")

	//KONSEP DETAIL PASS BY REFERENCE
	var address5 Address = Address{"Subang", "Jawa Barat", "Indonesia"}
	var address6 *Address = &address5 // Kalau begini dia mereference ke address 5

	address6.City = "Bandung"                                         // Ini jadinya akan ubah bandung ke address5 juga karena masih 1 address yang sama
	address6 = &Address{"Pontianak", "Kalimantan Barat", "Indonesia"} // Ini jadinya dia buat address baru untuk assign data address6

	fmt.Println(address5) //  address.city disini tidak akan mengikuti value address6 karena address6 sudah mempunyai address sendiri
	fmt.Println(address6)
	fmt.Println("====================================")

	//KONSEP DETAIL PASS BY REFERENCE
	var address7 Address = Address{"Subang", "Jawa Barat", "Indonesia"}
	var address8 *Address = &address7 // Kalau begini dia menset variable address8 yang bertipe data pointer menjadi address7, jadi kalo address7 diubah nanti jadinya keubah jg disini
	var address8b *Address = &address7
	// var address8c Address = address7

	// fmt.Println(address8c)
	databaru := address7 //Ini kita copy datanya, bukan set addressnya, jadi pas diubah pointernya maka ga akan ada pengaruh

	address8.City = "Bandung"                                         // Ini jadinya akan ubah bandung ke address7 juga karena masih 1 address yang sama
	*address8 = Address{"Pontianak", "Kalimantan Barat", "Indonesia"} // Ini akan arahin refer ke memory di address yang Subang jadi ke address baru isinya Pontianak

	// fmt.Println(&address7)
	fmt.Println(address7)         //  address7 akan menjadi Address{"Pontianak", "Kalimantan Barat", "Indonesia"}
	fmt.Printf("%p\n", &address7) // akan menampilkan pointer addressnya
	fmt.Println(address8)         // akan menjadi Address{"Pontianak", "Kalimantan Barat", "Indonesia"}
	fmt.Println(address8b)        // akan menjadi Address{"Pontianak", "Kalimantan Barat", "Indonesia"}
	fmt.Printf("%p\n", &address8) // akan menampilkan pointer addressnya
	fmt.Println(&address8b)       // akan menampilkan pointer addressnya
	fmt.Println(databaru)
	fmt.Printf("%p\n", &databaru)

	var addressbaru *Address = new(Address)
	fmt.Println(addressbaru)
	fmt.Printf("%p\n", &addressbaru)
}
