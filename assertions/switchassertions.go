package main

import (
	"fmt"
)

func random() interface{} {
	return "OK"
}

func main() {
	result := random()
	switch value := result.(type) { // Mirip switch case tapi dia cocokin type dengan casenya (type hanya bisa dipanggil di switch case)
	case string:
		fmt.Println("Value", value, "is string")
	case int:
		fmt.Println("Value", value, "is int")
	default:
		fmt.Println("Unknown type")
	}
}
