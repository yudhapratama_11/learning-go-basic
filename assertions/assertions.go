package main

import (
	"fmt"
)

func random() interface{} {
	return "OK"
}

func main() {
	result := random()
	resultString := result.(string) // konversi dari tipe data interface{} menjadi string

	fmt.Println(resultString)

	resultInt := result.(int)
	fmt.Println(resultInt) // Panic karena tipe datanya harusnya string tapi malah jadi integer

}
