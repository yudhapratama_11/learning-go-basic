package main

import (
	"fmt"
)

type Man struct {
	Name string
}

func Marrieds(man *Man) {
	man.Name = "Mr." + man.Name
}

func (man *Man) Gelar() {
	man.Name = man.Name + " S.Kom."
}

func main() {
	var yudha Man = Man{"Yudha"}
	yudha.Gelar()

	fmt.Println(yudha)

	var test Man = Man{"test"}
	Marrieds(&test)

	fmt.Println(test)
}
