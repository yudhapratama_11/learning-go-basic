package main

import (
	"container/ring"
	"fmt"
	"strconv"
)

func main() {
	data := ring.New(5) // Mirip linked list tapi next dari yg data yg terakhir diarahin ke list pertama
	for i := 0; i < data.Len(); i++ {
		data.Value = "Value " + strconv.FormatInt(int64(i), 10)
		data = data.Next()
	}

	data.Do(func(value interface{}) { // untuk memanggil data di ring
		fmt.Println(value)
	})

	// for i := 0; i < data.Len(); i++ {
	// 	fmt.Println(data.Value)
	// 	data = data.Next()
	// }

	data1 := ring.New(5)
	data1.Value = "Yudha"

	data2 := data1.Next()
	data2.Value = "Pratama"

	fmt.Println(data1.Value)
	fmt.Println(data2.Value)
}
