package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println(math.Round(1.7)) // Membulatkan nilai ke angka terdekat
	fmt.Println(math.Round(1.3)) // Membulatkan nilai ke angka terdekat
	fmt.Println(math.Floor(1.7)) // Membulatkan nilai ke angka dibawah
	fmt.Println(math.Ceil(1.7))  // Membulatkan nilai ke angka keatas
}
