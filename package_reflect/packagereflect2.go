package main

import (
	"fmt"
	"reflect"
	"strconv"
)

type Sample2 struct {
	Name string `required:"true" max:"10"`
}

type ContohLagi struct {
	Name        string `required:"true" max:"10"`
	Description string
}

func isValid(data interface{}) bool {
	t := reflect.TypeOf(data)
	for i := 0; i < t.NumField(); i++ { // ngecek semua attribute di struct (name, dll kalo ada)
		field := t.Field(i)
		if field.Tag.Get("required") == "true" {
			if reflect.ValueOf(data).Field(i).Interface() == "" {
				return false
			}
			// return reflect.ValueOf(data).Field(i).Interface() != "" // "" != ""
		}
		if field.Tag.Get("max") != "" { // Cek Tag Max ada atau engga
			str := reflect.ValueOf(data).Field(i).Interface().(string)
			max, _ := strconv.Atoi(reflect.TypeOf(data).Field(i).Tag.Get("max")) // Sama ky parseint
			// fmt.Println(max)
			// fmt.Println(len(str))
			if len(str) > max {
				return false
			}
		}
	}
	return true
}

func main() {
	sample := Sample2{"YudhaPrataa"}

	fmt.Println(isValid(sample))

	contohLagi := ContohLagi{"", ""}
	fmt.Println(isValid(contohLagi))
}
