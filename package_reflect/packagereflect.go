package main

import (
	"fmt"
	"reflect"
)

type Sample struct {
	Name string
}

func main() {
	sample := Sample{"Yudha"}

	sampleType := reflect.TypeOf(sample)
	fmt.Println(sampleType.NumField())

	structField := sampleType.Field(0)
	fmt.Println(structField.Name)
}
