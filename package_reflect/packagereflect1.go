package main

import (
	"fmt"
	"reflect"
)

type Sample1 struct {
	Name string `required:"true" max:"10"`
}

func main() {
	sample := Sample1{"Yudha"}

	sampleType := reflect.TypeOf(sample)
	structField := sampleType.Field(0)
	required := structField.Tag.Get("required")
	required1 := structField.Tag.Get("max")
	fmt.Println(required)
	fmt.Println(required1)
}
