package main

import (
	"fmt"
	"sort"
)

type User struct {
	Name string
	Age  int
}

type UserSlice []User

func (value UserSlice) Len() int {
	return len(value)
}

func (value UserSlice) Less(i, j int) bool {
	return value[i].Age < value[j].Age
}

func (value UserSlice) Swap(i, j int) {
	value[i], value[j] = value[j], value[i]

	// valtemp := value[i] //pake cara ini jg bisa
	// value[i] = value[j]
	// value[j] = valtemp
}

func main() {
	users := []User{
		{"Yudha", 22},
		{"Andi", 21},
		{"Budi", 41},
		{"Joko", 21},
		{"Rudi", 16},
	}

	number := []int{14, 51, 6, 1323, 22}

	sort.Ints(number)

	fmt.Println(number)

	// sort.SliceStable(users, func(i, j int) bool {
	// 	return users[i].Age < users[j].Age
	// })

	sort.Sort(UserSlice(users))

	//lengthUser := UserSlice(users).Len()
	//fmt.Println(lengthUser)

	fmt.Println(users)
}
