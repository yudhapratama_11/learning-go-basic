package main

import "fmt"

func main() {
	const firstName string = "Yudha"
	const lastName string = "Pratama"
	const unusedConst string = "Test" // Const tidak masalah kalo tidak dipakai

	fmt.Println(firstName + " " + lastName)

	const value = 1000
	fmt.Println(value)

	// Declare Multiple Constanta
	const (
		text1 = "Belajar"
		text2 = "Golang"
	)
	fmt.Println(text1 + " " + text2)
}
