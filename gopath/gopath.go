package main

import (
	"fmt"
)

type Man struct {
	Name string
}

func (man *Man) Married() {
	man.Name = "Mr." + man.Name
}

func main() {
	var yudha Man = Man{"Yudha"}
	yudha.Married()

	fmt.Println(yudha)
}
