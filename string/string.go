package main

import "fmt"

func main() {
	fmt.Println("Yudha")
	fmt.Println("Yudha Pratama")

	fmt.Println(len("Yudha")) // Mirip strlen
	fmt.Println("Yudha Pratama"[0])
	fmt.Println("Yudha Pratama"[1]) // Hasilnya jadi byte dari Huruf tsb. misal Y itu bytenya 117
}
