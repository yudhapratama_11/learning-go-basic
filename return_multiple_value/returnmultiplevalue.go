package main

import "fmt"

func main() {
	firstName, lastName := getFullName()
	// pertama, _ := getFullName

	fmt.Println(firstName, lastName)
}

func getFullName() (string, string) {
	return "Yudha", "Pratama"
}
