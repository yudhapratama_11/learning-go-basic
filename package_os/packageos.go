package main

import (
	"fmt"
	"os"
)

func main() {
	args := os.Args // ambil argument cth go run package.os yudha pratama , kalo mau ambil yudha ntar jadi args[1]
	fmt.Println(args)
	//fmt.Println(args[1])

	hostname, err := os.Hostname()
	if err == nil {
		fmt.Println("Hostname: ", hostname)
	} else {
		fmt.Println("Error: ", err)
	}

	username := os.Getenv("USERNAME") // Harus set environment dulu atau harus udah ada
	password := os.Getenv("PASSWORD") // Harus set environment dulu atau harus udah ada

	fmt.Println(username)
	fmt.Println(password)
}
