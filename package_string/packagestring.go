package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(strings.Contains("Yudha Pratama", "Yudha"))
	fmt.Println(strings.Split("Yudha Pratama", " "))
	fmt.Println(strings.ToLower("Yudha Pratama"))
	fmt.Println(strings.ToUpper("Yudha Pratama"))
	fmt.Println(strings.Trim("    Yudha Pratama      ", " "))
	fmt.Println(strings.ReplaceAll("Yudha Yudha Yudha     Yudha", "Yudha", "Budi"))
	fmt.Println(strings.ToTitle("yudha pratama"))
}
