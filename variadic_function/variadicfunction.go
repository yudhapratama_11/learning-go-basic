package main

import "fmt"

func main() {
	value := sumAll(1, 2, 3, 4, 5, 6, 7, 8, 9, 10) // Kalau dideklarasi langsung
	fmt.Println(value)

	numbers := []int{1, 2, 3, 4, 5, 6, 7} // Kalau menggunakan slice
	total := sumAll(numbers...)           // Kalau menggunakan slice
	fmt.Println(total)
}

func sumAll(numbers ...int) int { // fungsi return multiple value
	total := 0
	for _, number := range numbers {
		// fmt.Println(index)
		total += number
	}

	return total
}
