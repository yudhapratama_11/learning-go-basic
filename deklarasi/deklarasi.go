package main

import "fmt"

func main() {
	type NoKtp string
	type Married bool

	var ktp NoKtp = "6110231230123"
	var marriedStatus Married = false

	fmt.Println(ktp)
	fmt.Println(marriedStatus)
}
