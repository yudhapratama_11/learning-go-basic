package main

import "fmt"

func ifaceKosong(i int) interface{} {
	if i == 1 {
		return 1
	} else if i == 2 {
		return true
	} else {
		return false
	}
}

func main() {
	kosong := ifaceKosong(2)
	fmt.Println(kosong)
}
