package main

import (
	"fmt"
	"regexp"
)

func main() {
	var regex = regexp.MustCompile(`y([a-z|A-Z])(o|a)`) // Untuk memasukan tipe regular expressionnya (syaratnya)

	fmt.Println(regex.MatchString("yuo"))
	fmt.Println(regex.MatchString("yka"))
	fmt.Println(regex.MatchString("yKo"))

	search := regex.FindAllString("yuo yka yak yKo", -1)
	fmt.Println(search)
}
