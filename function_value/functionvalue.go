package main

import "fmt"

func main() {
	goodbye := getGoodBye // Function bisa disimpan di variable

	result := goodbye("Yudha")

	fmt.Println(result)
	fmt.Println(goodbye("Yudha"))
}

func getGoodBye(name string) string {
	return "Good bye " + name
}
