package main

import "fmt"

type Blacklist func(string) bool

func main() {
	value := factorialLoop(5)
	recursiveValue := recursiveFunction(5)

	fmt.Println(value)
	fmt.Println(recursiveValue)
}

func factorialLoop(value int) int {
	result := 1

	for i := value; i > 0; i-- {
		result = result * i
		// result *= i
	}
	return result
}

func recursiveFunction(value int) int {
	if value == 1 {
		return 1
	} else {
		return value * recursiveFunction(value-1) // 5 4 3 2 1
	}

}
