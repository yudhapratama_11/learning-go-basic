package helper // kalau dalam 1 package gaboleh fungsi dengan nama yang sama dengan file lain

import "fmt"

var version = "1.0.0"      //tidak bisa diakses dari luar karena huruf kecil diawal
var Application = "Golang" // bisa diakses dari luat karena huruf kapital diawal

func sayGoodBye(name string) string {
	return "Hello " + name
}

func SayHello(name string) {
	fmt.Println("Hello", name)
}
