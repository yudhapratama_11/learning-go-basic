package main

import "fmt"

func main() {
	firstName, middleName, lastName := getCompleteName()

	fmt.Println(firstName)
	fmt.Println(middleName)
	fmt.Println(lastName)
}

func getCompleteName() (firstName, middleName, lastName string) { // fungsi return multiple value
	firstName = "Yudha"
	middleName = "Kagane"
	lastName = "Pratama"

	return // return kosong juga bisa
	// return firstName, middleName, lastName // bisa pake ini juga
}
