package helper // kalau dalam 1 package gaboleh fungsi dengan nama yang sama dengan file lain

import "fmt"

func HelloPrivate(name string) {
	fmt.Println("Hello", name)
}
